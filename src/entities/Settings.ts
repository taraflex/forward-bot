import * as rndBytes from 'random-bytes-seed';
import { Column, Repository } from 'typeorm';

import { CoreSettings } from '@entities/CoreSettings';
import { SingletonEntity } from '@ioc';
import { STRING_SEED } from '@utils/rnd';

const pureSet = new Set<number>();
const rndGen = rndBytes(STRING_SEED);
while (pureSet.size < 11) {
    rndGen(22).forEach(b => pureSet.add(b));
}

@SingletonEntity()
export class Settings extends CoreSettings {

    @Column({ default: rndGen(256 / 8).toString('base64') })
    chachakey: string;

    @Column({ default: rndGen(96 / 8).toString('base64') })
    chachaiv: string;

    @Column({ default: Array.from(pureSet).slice(0, 11).join(','), type: 'simple-array' })
    pure: string[];

    @Column({ type: 'bigint', default: 0 })
    activeChat: number;

    @Column({ type: 'bigint', default: 0 })
    channel: number;
}

export abstract class SettingsRepository extends Repository<Settings>{ }