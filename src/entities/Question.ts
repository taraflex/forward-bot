import { Column, Entity, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { RTTIItemState } from '@crud/types';

export const ANY_RESPONSE = '<any>';

@Access({ _: 0 }, { display: 'table', icon: 'question-circle', order: 10 })
@Entity()
export class Question {
    @PrimaryGeneratedColumn()
    id: number;

    @v({ min: 1, type: 'md', state: RTTIItemState.FULLWIDTH })
    @Column({ default: '' })
    text: string;

    @v({ type: 'files', uploadUrl: 'tgimage', placeholder: 'Add image', uploadAccept: 'image/*' })
    @Column({ default: '[]', type: 'simple-json' })
    images: { name: string, url: string }[];

    @v({ enum: ['Да', 'Нет', ANY_RESPONSE, '1', '2'], state: RTTIItemState.ALLOW_CREATE, min: 1 })
    @Column({ default: '[]', type: 'simple-json' })
    posibleAnswers: string[];
}

export abstract class QuestionRepository extends Repository<Question>{ }