import { Column, Entity, PrimaryColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { RTTIItemState } from '@crud/types';

@Access({ GET: 0, DELETE: 0 }, { icon: 'comments', order: 15 })
@Entity()
export class Dialog {
    @v({ state: RTTIItemState.HIDDEN })
    @PrimaryColumn({ type: 'bigint', default: 0 })
    chat: number;

    @Column({ type: 'int', unique: true })
    invite: number;

    @Column({ default: '' })
    username: string;

    @Column({ default: '' })
    firstname: string;

    @Column({ default: '' })
    lastname: string;

    @Column({ default: false })
    canInterview: boolean;

    @v({ type: 'link', placeholder: 'Анкета ↗' })
    @Column()
    link: string;

    @v({ state: RTTIItemState.HIDDEN })
    @Column({ default: '[]', type: 'simple-json' })
    answers: string[];
}

export abstract class DialogRepository extends Repository<Dialog>{ }