import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { SingletonEntity } from '@ioc';

@Access({ GET: 0, PATCH: 0 }, { icon: 'brain', order: 5 })
@SingletonEntity()
export class AiBotSettings {

    @PrimaryGeneratedColumn()
    id: number;

    @v({ pattern: /^\d{9}:[\w-]{35}$/, description: 'Токен телеграм бота [ℹ️ справка](https://www.youtube.com/watch?v=W0f66uie9e8)' })
    @Column({ default: '' })
    telegramBotToken: string;

    @v({ notEqual: 0, description: 'Телеграм id оператора бота. Узнать id можно с помощью бота [@myidbot](tg://resolve?domain=myidbot)' })
    @Column({ type: 'bigint', default: 0 })
    operatorId: number;

    @v({ min: 1, description: 'Приветствие для ***не*** прошедших анкетирование. Тут стоит указать ссылку на бота-журналиста _https://t.me/<имя бота>_', type: 'md' })
    @Column({
        default: `Приветствую 🖖.
Сначала пройдите анкетирование. [<имя бота>](https://t.me/<имя бота>)`
    })
    welcomeMessageRejected: string;

    @v({ min: 1, description: 'Приветствие для прошедших анкетирование', type: 'md' })
    @Column({ default: 'Приветствую 🖖' })
    welcomeMessageAccepted: string;
}

export abstract class AiBotSettingsRepository extends Repository<AiBotSettings>{ }