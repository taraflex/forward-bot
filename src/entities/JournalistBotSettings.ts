import { Column, PrimaryGeneratedColumn, Repository } from 'typeorm';

import { Access, v } from '@crud';
import { SingletonEntity } from '@ioc';

@Access({ GET: 0, PATCH: 0 }, { icon: 'cog', order: 1 })
@SingletonEntity()
export class JournalistBotSettings {

    @PrimaryGeneratedColumn()
    id: number;

    @v({ pattern: /^\d{9}:[\w-]{35}$/, description: 'Токен телеграм бота [ℹ️ справка](https://www.youtube.com/watch?v=W0f66uie9e8)' })
    @Column({ default: '' })
    telegramBotToken: string;

    @v({ min: 1, description: 'Приветствие для тех, у кого сработал автоввод инвайта по ссылке (срабатывание зависит от фаз марсианских лун), или тех, кто ввел инвайт вручную, но зачем-то продолжает писать в чат комманду _/start_', type: 'md' })
    @Column({
        default: `Приветствую 🖖. 
Пройдите анкетирование.
Вспомогательные команды:
\`/next_question     \` - следующий вопрос
\`/prev_question     \` - предыдущий вопрос
\`/goto номер_вопроса\` - перейти к произвольному вопросу
` })
    welcomeMessageAccepted: string;

    @v({ min: 1, description: 'Приветствие для всех', type: 'md' })
    @Column({
        default: `Приветствую 🖖. 
Вероятно у вас не сработал автоввод инвайт кода по ссылке, 
либо инвайт код невалиден или уже был использован другим пользователем.
Введите в чат комманду \`/start ваш_инвайт_код\``
    })
    welcomeMessageRejected: string;

    @v({ min: 1, description: 'Сообщение после окончания анкетирования. Тут стоит указать ссылку на "ИИ" бота _https://t.me/<имя бота>_', type: 'md' })
    @Column({ default: 'Усё. Анкетирование закончено. [<имя бота>](https://t.me/<имя бота>)' })
    finalMessage: string;
}

export abstract class JournalistBotSettingsRepository extends Repository<JournalistBotSettings>{ }