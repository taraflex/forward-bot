import { EntitySubscriberInterface, EventSubscriber, UpdateEvent } from 'typeorm';
import { Container } from 'typescript-ioc';

import { AiBotSettings } from '@entities/AiBotSettings';

import { AIBot } from '../AIBot';

@EventSubscriber()
export class AiBotSettingsSubscriber implements EntitySubscriberInterface<AiBotSettings> {

    listenTo() {
        return AiBotSettings;
    }

    async beforeUpdate({ entity }: UpdateEvent<AiBotSettings>) {
        try {
            await (Container.get(AIBot) as AIBot).update(entity.telegramBotToken)
        } catch (err) {
            LOG_2ERRORS(entity.telegramBotToken, err);
            throw [{
                field: 'telegramBotToken',
                message: 'Invalid telegram token'
            }];
        }
    }
}