import { EntitySubscriberInterface, EventSubscriber, UpdateEvent } from 'typeorm';
import { Container } from 'typescript-ioc';

import { JournalistBotSettings } from '@entities/JournalistBotSettings';

import { JournalistBot } from '../JournalistBot';

@EventSubscriber()
export class JBotSettingsSubscriber implements EntitySubscriberInterface<JournalistBotSettings> {

    listenTo() {
        return JournalistBotSettings;
    }

    async beforeUpdate({ entity }: UpdateEvent<JournalistBotSettings>) {
        try {
            await (Container.get(JournalistBot) as JournalistBot).update(entity.telegramBotToken)
        } catch (err) {
            LOG_2ERRORS(entity.telegramBotToken, err);
            throw [{
                field: 'telegramBotToken',
                message: 'Invalid telegram token'
            }];
        }
    }
}