import { File } from 'formidable';
import { unlink } from 'fs-extra';
import { Context } from 'koa';
import * as koaBody from 'koa-body';
import { compileTemplate } from 'pug';
import { Inject, Singleton } from 'typescript-ioc';

import { AiBotSettings } from '@entities/AiBotSettings';
import { DialogRepository } from '@entities/Dialog';
import { JournalistBotSettings } from '@entities/JournalistBotSettings';
import { QuestionRepository } from '@entities/Question';
import { MAX_PAYLOAD_SIZE } from '@middlewares/body-parse-msgpack';
import noStore from '@middlewares/no-store';
import { promiseRequest } from '@utils/cancelable-request';
import { HOOKS } from '@utils/config';
import { InviteManager } from '@utils/InviteManager';
import { applyRoutes, toRouter } from '@utils/routes-helpers';

import { AIBot } from './AIBot';
import { AdminRouter } from './core/AdminRouter';
import { MainRouter } from './core/MainRouter';
import { JournalistBot } from './JournalistBot';

@Singleton
export class App {
    constructor(
        @Inject private readonly aiBot: AIBot,
        @Inject private readonly aiBotSettings: AiBotSettings,
        @Inject private readonly jBot: JournalistBot,
        @Inject private readonly jBotSettings: JournalistBotSettings,
        @Inject mainRouter: MainRouter,
        @Inject adminRouter: AdminRouter,
        @Inject inviteManager: InviteManager,
        @Inject dialogRepository: DialogRepository,
        @Inject questionRepository: QuestionRepository
    ) {
        adminRouter.get('invite', '/invite', noStore, async (ctx: Context) => {
            ctx.type = 'json';
            if (!jBot.botInfo) {
                throw {
                    status: 418,
                    message: 'Перед созданием инвайта задайте токены ботов (username бота будет частью инвайт ссылки)'
                };
            }
            const code = await inviteManager.generate();
            const link = `https://tele.click/${jBot.botInfo.username}?start=${code}`;
            ctx.body = JSON.stringify({
                code,
                link,
                shortLink: (await promiseRequest('GET', 'https://clck.ru/--?url=' + encodeURIComponent(link))).body.toString()
            });
        });

        const answersTemplate: compileTemplate = require('./answers.pug');

        adminRouter.get('user-dialog', '/dialog/:id', async (ctx: Context) => {
            const { answers } = await dialogRepository.findOneOrFail(ctx.params.id);
            const questions = await questionRepository.find({
                order: {
                    id: 'ASC'
                },
                take: answers.length
            });
            ctx.pug(answersTemplate, {
                questions,
                answers,
                entry: 'answers'
            });
        });

        adminRouter.post('tgimage', '/tgimage', koaBody({
            formLimit: MAX_PAYLOAD_SIZE,
            multipart: true,
            text: false,
            json: false
        }), async (ctx: Context) => {
            let file: File;
            ctx.type = 'text';
            try {
                file = ctx.request.files.file;
                ctx.body = await jBot.uploadImage(file.path);
            } catch (err) {
                ctx.status = 500;
                ctx.body = 'Ошибка загрузки изображения';
                LOG_ERROR(err);
            } finally {
                if (file) {
                    unlink(file.path).catch(err => LOG_ERROR(err));
                }
            }
        });

        const botRouter = toRouter(this.aiBot);
        applyRoutes(botRouter, this.jBot);
        mainRouter.use(HOOKS, botRouter.routes(), botRouter.allowedMethods());
    }

    init() {
        return Promise.all([
            this.jBotSettings.telegramBotToken && this.jBot.update(this.jBotSettings.telegramBotToken),
            this.aiBotSettings.telegramBotToken && this.aiBot.update(this.aiBotSettings.telegramBotToken)
        ]);
    }
}