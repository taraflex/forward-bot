import { Input } from 'element-ui';
import { toDataURL } from 'qrcode';
import Component, { mixins } from 'vue-class-component';

import { ErrorHandlerMixin } from '@frontend/error-handler-mixin';
import { httpJson } from '@utils/axios-smart';

@Component({
    components: { Input }
})
export default class InviteGenerator extends mixins(ErrorHandlerMixin) {
    $refs!: {
        area: Input
    }
    static get icon() {
        return 'qrcode';
    }
    code: string = '';
    link: string = '';
    shortLink: string = '';
    qr: string = '';
    shortQr: string = '';
    async generate() {
        this.loading = true;
        this.shortLink = '';
        this.code = '';
        this.link = '';
        try {
            const { data } = await httpJson.get(Paths.invite);
            this.code = data.code;
            this.link = data.link;
            this.shortLink = data.shortLink;

            const [qr, shortQr] = await Promise.all([
                toDataURL(data.link),
                toDataURL(data.shortLink)
            ]);
            this.qr = qr;
            this.shortQr = shortQr;

            this.$refs.area.focus();

            try {
                //@ts-ignore
                await navigator.clipboard.writeText(this.link);
            } catch (_) {
                try {
                    this.$refs.area.select();
                    if (!document.execCommand('copy')) {
                        //@ts-ignore
                        const { clipboardData } = window;
                        clipboardData && clipboardData.setData('Text', this.link);
                        if (!clipboardData || clipboardData.getData('Text') !== this.link) {
                            throw 'Доступ к буферу обмена запрещен.';
                        }
                    }
                } finally {
                    //reset selection
                    this.$refs.area.blur();
                    this.$refs.area.focus();
                }
            }
            this.$notify.success(<any>{
                message: 'Полная ссылка скопирована в буфер обмена.',
                position: 'bottom-right',
                duration: 3000
            });
        } catch (err) {
            this.onError(err);
        } finally {
            this.loading = false;
        }
    }
}