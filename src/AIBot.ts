import * as TelegramBot from 'node-telegram-bot-api';
import { Inject, Singleton } from 'typescript-ioc';

import { AiBotSettings } from '@entities/AiBotSettings';
import { Dialog, DialogRepository } from '@entities/Dialog';
import { SettingsRepository } from '@entities/Settings';
import { BaseTelegramBot, isCantWriteError, startCmdPayload } from '@utils/BaseTelegramBot';
import stringify from '@utils/stringify';
import trim from '@utils/trim';

function fullname(user: Dialog) {
    return trim([user.firstname, user.lastname, user.username].join(' ').replace(/[\s@]+/g, ' '));
}

@Singleton
export class AIBot extends BaseTelegramBot {

    private latestWaitMessage: TelegramBot.Message = null;

    constructor(
        @Inject private readonly botSettings: AiBotSettings,
        @Inject private readonly settingsRepository: SettingsRepository,
        @Inject private readonly dialogsRepository: DialogRepository
    ) {
        super();
        this.allowedUpdates.add('message');
        this.allowedUpdates.add('edited_message');
        this.allowedUpdates.add('callback_query');
    }

    formatActiveDialog(d: Dialog) {
        return `Активный диалог: [${fullname(d)}](${d.link})`;
    }

    private async showDialogsList(text: string = '') {
        let inline_keyboard: any[][] = [];
        let activeDialogMessage = text + 'Нет активного диалога.';
        for (let dialog of await this.dialogsRepository.find()) {
            if (dialog.chat != this.botSettings.operatorId) {
                if (dialog.chat == this.activeChat) {
                    activeDialogMessage = text + this.formatActiveDialog(dialog);
                }
                const btn = { text: fullname(dialog), callback_data: dialog.chat }
                const a = inline_keyboard[inline_keyboard.length - 1];
                if (a && a.length < 2) {
                    a.push(btn);
                } else {
                    inline_keyboard.push([btn]);
                }
            }
        }
        await this.sendKeyboard(this.botSettings.operatorId, activeDialogMessage);
        if (inline_keyboard.length > 0) {
            await this.bot.sendMessage(this.botSettings.operatorId, 'Все диалоги:', {
                disable_notification: true,
                reply_markup: {
                    inline_keyboard
                },
                parse_mode: 'Markdown'
            });
        }
    }

    private sendKeyboard(chat: number, text: string) {
        return this.bot.sendMessage(chat, text, {
            disable_web_page_preview: true,
            disable_notification: true,
            reply_markup: {
                resize_keyboard: true,
                keyboard: [[{ text: '/show_dialogs' }]]
            },
            parse_mode: 'Markdown'
        });
    }

    private set activeChat(chat: number) {
        if (chat != this.settings.activeChat) {
            this.latestWaitMessage = null;
            this.settings.activeChat = chat;
        }
    }

    private get activeChat() {
        return this.settings.activeChat;
    }

    private async changeActiveChat(dialog: Dialog) {
        const { chat } = dialog;
        const needUpdateSettings = chat && chat != this.activeChat;
        try {
            if (needUpdateSettings) {
                this.activeChat = chat;
            }
            if (this.botSettings.operatorId) {
                await this.bot.sendMessage(this.botSettings.operatorId, this.formatActiveDialog(dialog), {
                    disable_web_page_preview: true,
                    parse_mode: 'Markdown'
                });
            }
        } finally {
            if (needUpdateSettings) {
                await this.settingsRepository.save(this.settings);
            }
        }
    }

    private async sendError(err) {
        try {
            LOG_ERROR(err);
            const { operatorId } = this.botSettings;
            if (operatorId) {
                await this.bot.sendMessage(operatorId, stringify(err), { disable_web_page_preview: true });
            }
        } catch (_) { }
    }

    private isAdminMessage(message: TelegramBot.Message) {
        const chat = message.chat.id;
        const isAdmin = chat === this.botSettings.operatorId;
        if (isAdmin && this.activeChat === chat) {
            this.activeChat = 0;
        }
        return isAdmin;
    }

    protected async onMessage(message: TelegramBot.Message) {
        const isAdmin = this.isAdminMessage(message);
        try {
            const text = trim(message.text);
            const chat = message.chat.id;
            if (isAdmin && /^\/(show_dialogs|start)(\s|$)/.test(text)) {
                await this.showDialogsList();
            } else {
                if (isAdmin) {
                    if (!this.activeChat) {
                        throw 'Не выбран активный диалог.';
                    } else if (text) {
                        try {
                            await this.bot.editMessageText(text, {
                                disable_web_page_preview: true,
                                chat_id: this.latestWaitMessage.chat.id,
                                message_id: this.latestWaitMessage.message_id
                            });
                        } catch (_) {
                            await this.bot.sendMessage(this.activeChat, text, { disable_web_page_preview: true });
                        } finally {
                            this.latestWaitMessage = null;
                        }
                    } else {
                        throw 'Поддерживается перессылка только текстовых сообщений.';
                    }
                } else {
                    let dialog = await this.dialogsRepository.findOne(chat);
                    if (dialog && dialog.canInterview) {
                        if (!this.activeChat) {
                            await this.changeActiveChat(dialog);
                        }
                        if (startCmdPayload(text) != null) {
                            await this.bot.sendMessage(chat, this.botSettings.welcomeMessageAccepted, {
                                disable_web_page_preview: true,
                                parse_mode: 'Markdown'
                            });
                        } else {
                            const { operatorId } = this.botSettings;
                            const [latestWaitMessage] = await Promise.all([
                                this.bot.sendMessage(chat, 'Сообщение обрабатывается...', { disable_notification: true }),
                                (operatorId ? this.bot.forwardMessage(operatorId, chat, message.message_id) : null),
                            ]);
                            if (chat === this.activeChat) {
                                this.latestWaitMessage = latestWaitMessage;
                            }
                        }
                    } else {
                        await this.bot.sendMessage(chat, this.botSettings.welcomeMessageRejected, {
                            disable_web_page_preview: true,
                            parse_mode: 'Markdown'
                        });
                    }
                }
            }
        } catch (err) {
            if (isAdmin && isCantWriteError(err)) {
                await this.showDialogsList('Отправка сообщений в чат заблокирована. Выберите другой чат.\n');
            } else {
                await this.sendError(err);
            }
        }
    }

    protected async onEditedMessage(message: TelegramBot.Message) {
        try {
            if (this.isAdminMessage(message)) {
                throw 'Редактирование сообщений не поддерживается. Пользователь не увидит изменения вашего сообщения.';
            } else {
                const { operatorId } = this.botSettings;
                if (operatorId) {
                    await this.bot.forwardMessage(operatorId, message.chat.id, message.message_id);
                }
            }
        } catch (err) {
            await this.sendError(err);
        }
    }

    protected async onCallbackQuery(c: TelegramBot.CallbackQuery) {
        try {
            const chat = parseInt(c.data);
            await this.changeActiveChat(await this.dialogsRepository.findOne(chat));
        } catch (err) {
            await this.sendError(err);
        }
    }
}