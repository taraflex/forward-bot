export class Chain<T> {
    readResolve: (value?: T | PromiseLike<T>) => void;
    write(o: T) {
        if (this.readResolve) {
            try {
                this.readResolve(o);
            } finally {
                this.readResolve = null;
            }
        }
    }
    read() {
        return new Promise<T>(resolve => this.readResolve = resolve);
    }
}