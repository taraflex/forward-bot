import { createCipheriv, createDecipheriv } from 'crypto';
import { Container } from 'typescript-ioc';

import { Settings } from '@entities/Settings';

const encode32 = require('base32-encode');
const decode32 = require('base32-decode');

export class SimpleProtector {
    private readonly secret: Buffer = Buffer.from((Container.get(Settings) as Settings).chachakey, 'base64');
    private readonly iv: Buffer = Buffer.from((Container.get(Settings) as Settings).chachaiv, 'base64');

    encrypt(data: Uint8Array): string {
        const cipher = createCipheriv('chacha20-poly1305', this.secret, this.iv);
        return encode32(cipher.update(data), 'Crockford');
    }

    decrypt(s: string) {
        const data = Buffer.from(decode32(s.toUpperCase(), 'Crockford') as ArrayBuffer);
        const decipher = createDecipheriv('chacha20-poly1305', this.secret, this.iv);
        return decipher.update(data);
    }
}