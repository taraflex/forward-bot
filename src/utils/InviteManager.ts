import { Inject, Singleton } from 'typescript-ioc';

import { DialogRepository } from '@entities/Dialog';
import { Settings } from '@entities/Settings';
import { promiseRequest } from '@utils/cancelable-request';
import { SimpleProtector } from '@utils/SimpleProtector';

@Singleton
export class InviteManager extends SimpleProtector {
    private readonly posibleBytes: number[];
    private readonly posibleBytesSet: Set<number>;

    constructor(
        @Inject settings: Settings,
        @Inject private readonly dialogsRepositiry: DialogRepository
    ) {
        super();
        this.posibleBytesSet = new Set(this.posibleBytes = settings.pure.map(e => +e));
    }

    async generate() {
        const data = Buffer.allocUnsafe(4);
        let invite = 0;
        do {
            data[0] = this.posibleBytes[(Math.random() * 11) | 0];
            data[1] = this.posibleBytes[(Math.random() * 11) | 0];
            data[2] = this.posibleBytes[(Math.random() * 11) | 0];
            data[3] = this.posibleBytes[(Math.random() * 11) | 0];
            invite = data.readUInt32BE(0);
        } while (invite === 0 || await this.dialogsRepositiry.findOne({ invite }));
        return this.encrypt(data);
    }

    async check(s: string) {
        if (s) {
            try {
                if (s.length !== 7) {
                    let url = new URL(s);
                    if (url.hostname === 'clck.ru' || url.hostname === 'www.clck.ru') {
                        const { headers } = await promiseRequest('HEAD', {
                            url: s,
                            followAllRedirects: false,
                            followRedirect: false
                        });
                        url = new URL(headers.location);
                    }
                    s = url.searchParams.get('start');
                }
                if (!s || s.length !== 7) {
                    throw null;
                }
                const data = this.decrypt(s);
                if (
                    data.length === 4 &&
                    this.posibleBytesSet.has(data[0]) &&
                    this.posibleBytesSet.has(data[1]) &&
                    this.posibleBytesSet.has(data[2]) &&
                    this.posibleBytesSet.has(data[3])
                ) {
                    const invite = data.readUInt32BE(0);
                    if (!await this.dialogsRepositiry.findOne({ invite })) {
                        return invite;
                    }
                }
            } catch (_) { }
        }
        return 0;
    }
}