import { createReadStream } from 'fs';
import * as TelegramBot from 'node-telegram-bot-api';
import { Inject, Singleton } from 'typescript-ioc';

import { AiBotSettings } from '@entities/AiBotSettings';
import { Dialog, DialogRepository } from '@entities/Dialog';
import { JournalistBotSettings } from '@entities/JournalistBotSettings';
import { ANY_RESPONSE, QuestionRepository } from '@entities/Question';
import { SettingsRepository } from '@entities/Settings';
import {
    BaseTelegramBot, getCommandParser, isCantWriteError, startCmdPayload
} from '@utils/BaseTelegramBot';
import { Chain } from '@utils/Chain';
import { InviteManager } from '@utils/InviteManager';
import stringify from '@utils/stringify';
import trim from '@utils/trim';

function findEmpty(a: string[]) {
    for (let i = 0; i < a.length; i++) {
        if (!a[i]) {
            return i;
        }
    }
    return a.length;
}

const nextCmdPayload = getCommandParser('next_question');
const prevCmdPayload = getCommandParser('prev_question');
const gotoCmdPayload = getCommandParser('goto');

class DialogSession {
    userInput = new Chain<string>();
    constructor(private chat: number, private bot: JournalistBot) {
        this.process().catch(err => LOG_ERROR(err));
    }

    async processText(message_id: number, order: number, count: number, dialog: Dialog, answers: Set<string>, originalAnswers: string[], finished: boolean) {
        const text = trim(await this.userInput.read());
        if (startCmdPayload(text) != null) {
            await this.bot.welcome(this.chat);
            return order;
        } else if (prevCmdPayload(text) != null) {
            return Math.max(0, order - 1);
        } else if (nextCmdPayload(text) != null) {
            return Math.min(count - 1, order + 1);
        } else {
            const goto = gotoCmdPayload(text);
            if (goto == null) {
                if (!finished) {
                    if (answers) {
                        if (!answers.has(text.toLowerCase())) {
                            await this.bot.bot.sendMessage(this.chat, 'Разрешены только ответы: ' + originalAnswers.join(', '));
                            return null;
                        }
                    } else {
                        if (!text) {
                            await this.bot.bot.sendMessage(this.chat, 'Разрешены только непустые текстовые ответы.');
                            return null;
                        }
                    }
                    await Promise.all([
                        message_id && this.bot.bot.editMessageReplyMarkup({ inline_keyboard: [] }, { chat_id: this.chat, message_id }),
                        text != dialog.answers[order] && (dialog.answers[order] = text) && this.bot.dialogsRepository.save(dialog)
                    ]);
                }
            } else {
                const o = parseInt(goto) - 1;
                return o >= 0 ? o : order;
            }
        }
        return -1;
    }
    async process() {
        try {
            await this.bot.welcome(this.chat);
            let order = -1
            for (; ;) {
                try {
                    const [questionsCounts, dialog] = await Promise.all([
                        this.bot.questionsRepository.count(),
                        this.bot.dialogsRepository.findOne({ chat: this.chat })
                    ]);
                    if (order < 0 || order >= questionsCounts) {
                        order = findEmpty(dialog.answers);
                    }
                    const finished = order >= questionsCounts;
                    let answers: Set<string> = null;
                    let originalAnswers: string[] = null;
                    let message_id = 0;
                    if (!finished) {
                        const q = await this.bot.findQuestion(order);
                        if (!q.posibleAnswers.includes(ANY_RESPONSE)) {
                            answers = new Set((originalAnswers = q.posibleAnswers).map(s => s.toLowerCase()));
                        }
                        const lastAnswer = dialog.answers[order];
                        const inline_keyboard = q.posibleAnswers
                            .filter(a => a != ANY_RESPONSE)
                            .map((a, i) => [{
                                text: a,
                                callback_data: JSON.stringify({ q: order, i })
                            }]);

                        let i = 0;
                        for (let image of q.images) {
                            await this.bot.bot.sendPhoto(this.chat, image.url, {
                                disable_notification: true,
                                caption: (++i) + '.'
                            });
                        }

                        const message = await this.bot.bot.sendMessage(this.chat, lastAnswer ? `${order + 1}. ${q.text}

Ваш ответ: ${lastAnswer}` : `${order + 1}. ${q.text}`, {
                                disable_web_page_preview: true,
                                parse_mode: 'Markdown',
                                reply_markup: { inline_keyboard }
                            });
                        if (inline_keyboard.length > 0) {
                            message_id = message.message_id;
                        }
                    } else {
                        await Promise.all([
                            !dialog.canInterview && (dialog.canInterview = true) && this.bot.dialogsRepository.save(dialog),
                            this.bot.bot.sendMessage(this.chat, this.bot.jbotSettings.finalMessage, {
                                disable_web_page_preview: true,
                                parse_mode: 'Markdown'
                            })
                        ]);
                    }
                    while ((order = await this.processText(message_id, order, questionsCounts, dialog, answers, originalAnswers, finished)) === null) { }
                } catch (err) {
                    if (await this.bot.deleteDialogIfError(err, this.chat)) {
                        return;
                    } else {
                        LOG_ERROR(err);
                    }
                    order = -1;
                }
            }
        } finally {
            this.bot.sessions.delete(this.chat);
        }
    }
    write(m?: string) {
        this.userInput.write(m);
    }
}

@Singleton
export class JournalistBot extends BaseTelegramBot {

    public readonly sessions = new Map<number, DialogSession>();

    constructor(
        @Inject public readonly dialogsRepository: DialogRepository,
        @Inject public readonly questionsRepository: QuestionRepository,
        @Inject public readonly jbotSettings: JournalistBotSettings,
        @Inject private readonly settingsRepository: SettingsRepository,
        @Inject private readonly botSettings: AiBotSettings,
        @Inject private readonly inviteManager: InviteManager
    ) {
        super();
        this.allowedUpdates.add('message');
        this.allowedUpdates.add('channel_post');
        this.allowedUpdates.add('callback_query');
    }

    protected onChannelPost(message: TelegramBot.Message) {
        if (!this.settings.channel) {
            this.settings.channel = message.chat.id;
            this.settingsRepository.save(this.settings).catch(err => LOG_ERROR(err));
        }
    }

    async uploadImage(path: string) {
        const { photo } = await this.bot.sendPhoto(this.settings.channel, createReadStream(path), { disable_notification: true });
        return photo[0].file_id;
    }

    async deleteDialogIfError(err, chat: number) {
        if (isCantWriteError(err)) {
            try {
                await this.dialogsRepository.delete({ chat });
            } catch (e) {
                LOG_ERROR(e);
            } finally {
                // this.sessions.delete(chat)
                return true;
            }
        }
        return false;
    }

    welcome(chat: number) {
        return this.bot.sendMessage(chat, this.jbotSettings.welcomeMessageAccepted, {
            disable_web_page_preview: true,
            parse_mode: 'Markdown',
            disable_notification: true,
            reply_markup: {
                resize_keyboard: true,
                keyboard: [[{ text: '/prev_question' }, { text: '/next_question' }]]
            },
        });
    }

    private async sendError(err) {
        try {
            LOG_ERROR(err);
            const { operatorId } = this.botSettings;
            if (operatorId) {
                await this.bot.sendMessage(operatorId, stringify(err), { disable_web_page_preview: true });
            }
        } catch (_) { }
    }

    protected write(chat: number, info: string) {
        const session = this.sessions.get(chat);
        if (session) {
            session.write(info);
        } else {
            this.sessions.set(chat, new DialogSession(chat, this));
        }
    }

    protected async onMessage(message: TelegramBot.Message) {
        try {
            if (!message.from) {
                return;
            }
            const text = trim(message.text);
            const chat = message.chat.id;
            let dialog = await this.dialogsRepository.findOne(chat);
            if (!dialog) {
                const invite = await this.inviteManager.check(startCmdPayload(text));
                if (invite) {
                    dialog = new Dialog();
                    dialog.chat = chat;
                    dialog.link = this.settings.rootUrl + 'dialog/' + chat;
                    dialog.invite = invite;
                    dialog.firstname = message.from.first_name;
                    dialog.lastname = message.from.last_name;
                    dialog.username = message.from.username;
                    await this.dialogsRepository.insert(dialog);
                }
            }
            if (dialog) {
                this.write(dialog.chat, message.text);
            } else {
                await this.bot.sendMessage(chat, this.jbotSettings.welcomeMessageRejected, {
                    disable_web_page_preview: true,
                    parse_mode: 'Markdown'
                });
            }
        } catch (err) {
            if (!await this.deleteDialogIfError(err, message.chat.id)) {
                await this.sendError(err);
            }
        }
    }

    async findQuestion(n: number) {
        return (await this.questionsRepository.find({
            order: {
                id: 'ASC'
            },
            take: 1,
            skip: n
        }))[0];
    }

    protected async onCallbackQuery(c: TelegramBot.CallbackQuery) {
        try {
            let answer = c.data;
            if (answer.startsWith('{"') && answer.endsWith('}')) {
                const { q, i } = JSON.parse(c.data);
                const { posibleAnswers } = await this.findQuestion(q);
                answer = posibleAnswers[i];
            }
            this.write(c.message.chat.id, answer);
        } catch (err) {
            await this.sendError(err);
        }
    }
}